/*
 SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	pb "github.com/hyperledger/fabric-protos-go/peer"
)

type TodoChaincode struct {
}

type todo struct {
	ObjectType string `json:"docType"` //docType is used to distinguish the various types of objects in state database
	ID         string `json:"id"`
	Title      string `json:"title"`
	Done       bool   `json:"done"`
	Creator    string `json:"creator"`
}

func main() {
	err := shim.Start(new(TodoChaincode))
	if err != nil {
		fmt.Printf("Error starting Todo chaincode: %s", err)
	}
}

// Init initializes chaincode
func (t *TodoChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
func (t *TodoChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	switch function {
	case "create":
		return t.create(stub, args)
	case "read":
		return t.read(stub, args)
	case "update":
		return t.update(stub, args)
	case "delete":
		return t.delete(stub, args)
	case "setDone":
		return t.setDone(stub, args)
	case "queryTodos":
		return t.queryTodos(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")
}

// Create new todo
func (t *TodoChaincode) create(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//   0       1       		 2      	3
	// "71", "Buy pika coin", "false", "teamrocket"
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument must be a non-empty string")
	}

	var err error

	id := args[0]
	title := args[1]
	done, err := strconv.ParseBool(args[2])
	if err != nil {
		return shim.Error("3rd argument must be a boolean string")
	}
	creator := args[3]

	// Check if ID already exists
	todoAsBytes, err := stub.GetState(id)
	if err != nil {
		return shim.Error("Failed to get todo: " + err.Error())
	} else if todoAsBytes != nil {
		fmt.Println("This ID already exists: %v" + id)
		return shim.Error("This marble already exists: %v" + id)
	}

	// Create todo object and marshal to JSON
	objectType := "todo"
	todo := &todo{objectType, id, title, done, creator}
	todoJSONasBytes, err := json.Marshal(todo)
	if err != nil {
		return shim.Error(err.Error())
	}

	// Save todo to state
	err = stub.PutState(id, todoJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	// Index the todo to enable completion-based range queries, e.g. return all completed todo
	indexName := "done~title"
	doneTitleIndexKey, err := stub.CreateCompositeKey(indexName, []string{strconv.FormatBool(todo.Done), todo.Title})
	if err != nil {
		return shim.Error(err.Error())
	}
	value := []byte{0x00}
	stub.PutState(doneTitleIndexKey, value)
	fmt.Println("- end create todo")
	return shim.Success(nil)
}

// Read a todo from chaincode state
func (t *TodoChaincode) read(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var jsonResp string
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting todo ID.")
	}

	id := args[0]
	valAsbytes, err := stub.GetState(id)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for todo:" + id + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"Todo does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(valAsbytes)
}

// Update todo title
func (t *TodoChaincode) update(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//   0       1
	// "71", "james"
	if len(args) < 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	id := args[0]
	newTitle := args[1]
	fmt.Println("- start update todo title ", id, newTitle)

	todoAsBytes, err := stub.GetState(id)
	if err != nil {
		return shim.Error("Failed to get todo:" + err.Error())
	} else if todoAsBytes == nil {
		return shim.Error("Todo does not exist")
	}

	todoToUpdate := todo{}
	err = json.Unmarshal(todoAsBytes, &todoToUpdate) //unmarshal it aka JSON.parse()
	if err != nil {
		return shim.Error(err.Error())
	}
	todoToUpdate.Title = newTitle //change title

	todoJSONasBytes, _ := json.Marshal(todoToUpdate)
	err = stub.PutState(id, todoJSONasBytes) //rewrite todo
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("- end update todo")
	return shim.Success(nil)
}

// Delete a todo
func (t *TodoChaincode) delete(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var jsonResp string
	var todoJSON todo
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	id := args[0]

	// to maintain the done~title index, we need to read the todo first and get its title
	valAsbytes, err := stub.GetState(id) //get the todo from chaincode state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for todo:" + id + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"Todo does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}

	err = json.Unmarshal([]byte(valAsbytes), &todoJSON)
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to decode JSON of: " + id + "\"}"
		return shim.Error(jsonResp)
	}

	err = stub.DelState(id) //remove the todo from chaincode state
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}

	// maintain the index
	indexName := "done~title"
	doneTitleIndexKey, err := stub.CreateCompositeKey(indexName, []string{strconv.FormatBool(todoJSON.Done), todoJSON.Title})
	if err != nil {
		return shim.Error(err.Error())
	}

	//  Delete index entry to state
	err = stub.DelState(doneTitleIndexKey)
	if err != nil {
		return shim.Error("Failed to delete state:" + err.Error())
	}
	fmt.Println("- end delete todo")
	return shim.Success(nil)
}

// Change todo status to completed
func (t *TodoChaincode) setDone(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	id := args[0]

	todoAsBytes, err := stub.GetState(id)
	if err != nil {
		return shim.Error("Failed to get todo:" + err.Error())
	} else if todoAsBytes == nil {
		return shim.Error("Todo does not exist")
	}

	todoToUpdate := todo{}
	err = json.Unmarshal(todoAsBytes, &todoToUpdate) //unmarshal it aka JSON.parse()
	if err != nil {
		return shim.Error(err.Error())
	}
	todoToUpdate.Done = true // set done to true

	todoJSONasBytes, _ := json.Marshal(todoToUpdate)
	err = stub.PutState(id, todoJSONasBytes) //rewrite todo
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("- end update todo")
	return shim.Success(nil)
}

// Get all todos
func (t *TodoChaincode) queryTodos(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//   0
	// "queryString"
	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	queryString := args[0]

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return nil, err
	}

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}

// ===========================================================================================
// constructQueryResponseFromIterator constructs a JSON array containing query results from
// a given result iterator
// ===========================================================================================
func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) {
	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return &buffer, nil
}
